﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Orgil;

public class LevelBar : Mb {
    public Image bgImg, bstImg, scrImg, scrPtrImg, bstPtrImg, nxtLvlImg;
    public Text curLvlTxt, nxtLvlTxt, lvlTxt, dataTxt;
    public string format = "LEVEL (data)";
    public bool isStage = false;
    public Stage stgLvlPf, stgPf;
    public bool isCurNxtLvl = true;
    public float dSpc = 95, spc = 80;
    public void Data(int level, float progressScore, float progressBest, string progressText, int stage, bool isNxtLvlCol) {
        if (!isStage) {
            if (curLvlTxt.IsAct())
                curLvlTxt.text = level.tS();
            if (nxtLvlTxt.IsAct())
                nxtLvlTxt.text = (level + 1).tS();
            if (lvlTxt.IsAct())
                lvlTxt.text = format.Fmt("(data)", level);
            if (dataTxt.IsAct())
                dataTxt.text = progressText;
            if (bstImg.IsAct())
                bstImg.fillAmount = progressBest;
            if (scrImg.IsAct())
                scrImg.fillAmount = progressScore;
            if (scrPtrImg.IsAct() && bgImg.IsAct())
                scrPtrImg.rectTransform.anchoredPosition = new Vector2(bgImg.rectTransform.sizeDelta.x * (progressScore - 0.5f), scrPtrImg.rectTransform.anchoredPosition.y);
            if (bstPtrImg.IsAct() && bgImg.IsAct())
                bstPtrImg.rectTransform.anchoredPosition = new Vector2(bgImg.rectTransform.sizeDelta.x * (progressBest - 0.5f), bstPtrImg.rectTransform.anchoredPosition.y);
            if (isNxtLvlCol && progressScore >= 0.999f && nxtLvlImg.IsAct() && scrImg.IsAct())
                nxtLvlImg.color = scrImg.color;
        } else {
            Stage curLvlStg = null, nxtLvlStg = null;
            List<Stage> stages = new List<Stage>();
            go.DstChilds<Stage>();
            float dx = (1 - stage) * 0.5f * spc;
            if (isCurNxtLvl) {
                if (stgLvlPf) {
                    curLvlStg = Ins(stgLvlPf, Tp, Q.O, tf);
                    curLvlStg.Tlp = V3.r * (dx - dSpc);
                    curLvlStg.SetState(StageState.Done);
                    nxtLvlStg = Ins(stgLvlPf, Tp, Q.O, tf);
                    nxtLvlStg.Tlp = V3.r * (dx + (stage - 1) * spc + dSpc);
                }
            } else if (lvlTxt.IsAct())
                lvlTxt.text = format.Fmt("(data)", level);
            stages.Clear();
            for (int i = 0; i < stage; i++) {
                Stage stg = Ins(stgPf, Tp, Q.O, tf);
                stg.Tlp = V3.r * (dx + i * spc);
                stages.Add(stg);
            }
            if (curLvlStg && curLvlStg.levelTxt)
                curLvlStg.levelTxt.text = level.tS();
            if (nxtLvlStg && nxtLvlStg.levelTxt)
                nxtLvlStg.levelTxt.text = (level + 1).tS();
            int idx = (int)(stage * progressScore);
            if (curLvlStg)
                curLvlStg.SetState(StageState.Done);
            for (int i = 0; i < stage; i++)
                stages[i].SetState(i < idx ? StageState.Done : i == idx ? StageState.Selected : StageState.None);
            if (nxtLvlStg)
                nxtLvlStg.SetState(idx >= stage ? StageState.Done : StageState.None);
        }
    }
}