using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Mb : MonoBehaviour {
    public static T[] Arr<T>(params T[] arr) => arr;
    public static List<T> List<T>(params T[] arr) => arr.ToList();
    public static bool IsStarting => Gc.State == GameState.Starting;
    public static bool IsPlaying => Gc.State == GameState.Playing;
    public static bool IsPause => Gc.State == GameState.Pause;
    public static bool IsLevelCompleted => Gc.State == GameState.LevelCompleted;
    public static bool IsGameOver => Gc.State == GameState.GameOver;
    public static bool IsSettings => Gc.State == GameState.Settings;
    public static bool IsShifting => Gc.State == GameState.Shifting;
    public static bool IsMb => Input.GetMouseButton(0);
    public static bool IsMbD => Input.GetMouseButtonDown(0);
    public static bool IsMbU => Input.GetMouseButtonUp(0);
    public static Vector3 Mp => Input.mousePosition;
    [HideInInspector]
    public Vector3 mp;
    public static float Dt => Time.deltaTime;
    public GameObject go => gameObject;
    public Transform tf => transform;
    public Rigidbody rb { get { if (!_rb) _rb = gameObject.GetComponent<Rigidbody>(); return _rb; } }
    Rigidbody _rb;
    public Rigidbody2D rb2 { get { if (!_rb2) _rb2 = gameObject.GetComponent<Rigidbody2D>(); return _rb2; } }
    Rigidbody2D _rb2;
    public Transform P { get => transform.parent; set => transform.parent = value; }
    public GameObject PGo { get => transform.parent.gameObject; set => transform.parent = value.transform; }
    public int Tcc { get => transform.childCount; }
    public Vector3 Tp { get => transform.position; set => transform.position = value; }
    public Vector3 Tlp { get => transform.localPosition; set => transform.localPosition = value; }
    public Quaternion Tr { get => transform.rotation; set => transform.rotation = value; }
    public Quaternion Tlr { get => transform.localRotation; set => transform.localRotation = value; }
    public Vector3 Ts => transform.lossyScale;
    public Vector3 Tls { get => transform.localScale; set => transform.localScale = value; }
    public Vector3 Te { get => transform.eulerAngles; set => transform.eulerAngles = value; }
    public Vector3 Tle { get => transform.localEulerAngles; set => transform.localEulerAngles = value; }
    public Vector3 L => -transform.right;
    public Vector3 R => transform.right;
    public Vector3 D => -transform.up;
    public Vector3 U => transform.up;
    public Vector3 B => -transform.forward;
    public Vector3 F => transform.forward;
    public static T Ins<T>(T pf) where T : Object => Instantiate(pf);
    public static T Ins<T>(T pf, Vector3 pos, Quaternion rot) where T : Object => Instantiate(pf, pos, rot);
    public static T Ins<T>(T pf, Vector3 pos, Quaternion rot, Transform par) where T : Object => Instantiate(pf, pos, rot, par);
    public static T Ins<T>(T pf, Transform par) where T : Object => Instantiate(pf, par);
    public static T Ins<T>(T pf, Transform par, bool isWorldPosStays) where T : Object => Instantiate(pf, par, isWorldPosStays);
    public static void Dst(Object obj) => Destroy(obj);
    public static void Dst(Object obj, float t = 0f) => Destroy(obj, t);
    public void CxlIvk(string name) => CancelInvoke(name);
    public void CxlIvk() => CancelInvoke();
    public void Ivk(string name, float t) => Invoke(name, t);
    public void IvkRep(string name, float t, float rate) => InvokeRepeating(name, t, rate);
    public bool IsIvk(string name) => IsInvoking(name);
    public bool IsIvk() => IsInvoking();
    public Coroutine StaCor(string name) => StartCoroutine(name);
    public Coroutine StaCor(IEnumerator routine) => StartCoroutine(routine);
    public Coroutine StaCor(string name, object val = null) => StartCoroutine(name, val);
    public void StopCor() => StopAllCoroutines();
    public void StopCor(IEnumerator routine) => StopCoroutine(routine);
    public void StopCor(Coroutine routine) => StopCoroutine(routine);
    public void StopCor(string name) => StopCoroutine(name);
}