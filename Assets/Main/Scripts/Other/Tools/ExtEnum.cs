namespace Orgil {
	public static class ExtEnum {
		public static bool IsN(this Nzp a) { return a == Nzp.N; }
		public static bool IsZ(this Nzp a) { return a == Nzp.Z; }
		public static bool IsP(this Nzp a) { return a == Nzp.P; }
		public static bool IsC(this RepTp a) { return a == RepTp.C; }
		public static bool IsRep(this RepTp a) { return a == RepTp.Rep; }
		public static bool IsPingPong(this RepTp a) { return a == RepTp.PingPong; }
		public static bool IsRound(this RoundTp a) { return a == RoundTp.Round; }
		public static bool IsFloor(this RoundTp a) { return a == RoundTp.Floor; }
		public static bool IsCeil(this RoundTp a) { return a == RoundTp.Ceil; }
		public static bool IsO(this RoundTp a) { return a == RoundTp.O; }
		public static bool IsN(this RoundTp a) { return a == RoundTp.N; }
		public static bool IsDir(this RoundTp a) { return a == RoundTp.Dir; }
	}
}