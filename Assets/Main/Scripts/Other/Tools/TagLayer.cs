namespace Orgil {
	public enum Tag { Untagged, Respawn, Finish, EditorOnly, MainCamera, Player, GameController, Diamond, Bot, Coin, Dog, Room }
	public class Lyr { public const int Default = 0, TransparentFX = 1, IgnoreRaycast = 2, Dog = 3, Water = 4, UI = 5, Silde = 6, Door = 7, Floor = 8, Wall = 9; }
	public class Lm { public const int Default = 1, TransparentFX = 2, IgnoreRaycast = 4, Dog = 8, Water = 16, UI = 32, Silde = 64, Door = 128, Floor = 256, Wall = 512; }
}