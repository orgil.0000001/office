using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

[RequireComponent (typeof (Rigidbody))]
public class PlayerBlocksBusterController : Mb {
    public float speed = 10, posScl = 0.02f;
    public bool isPlaying = false;
    float dis;
    Vector3 pos;
    void Start () {
        rb.NoG();
        rb.Con(false, true, false, true, true, true);
    }
    void Update () {
        if (IsPlaying || isPlaying) {
            if (IsMbD)
                mp = Mp;
            if (IsMb) {
                pos = Tp + new Vector3 (Mp.x - mp.x, 0, Mp.y - mp.y) * posScl;
                rb.MovePosition (pos);
                mp = Mp;
            }
            if (IsMbU)
                rb.V0();
        } else
            rb.V0();
    }
}