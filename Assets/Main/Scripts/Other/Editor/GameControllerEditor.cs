using UnityEditor;
using UnityEngine;
using Orgil;

[CustomEditor(typeof(Gc))]
public class GameControllerEditor : Editor {
    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
        if (GUILayout.Button("Sprites Screenshot"))
            A.Screenshot("Assets/Main/Sprites", Gc.I.screenshotName);
        // if (GUILayout.Button("Screenshot"))
        //     A.Screenshot("Screenshots/", "Screenshot" + System.DateTime.Now.ToString("_yyyy-MM-dd_hh-mm-ss"));
        // if (GUILayout.Button("Icon Read")) {
        //     Gc.I.icon = A.LoadAsset<Texture2D>("Assets/Main/Sprites/icon.png");
        // }
        if (GUILayout.Button("Player Settings Change"))
            A.PlayerSettingsChange();
        // if (GUILayout.Button("Delete PlayerPrefs"))
        //     PlayerPrefs.DeleteAll();
    }
}