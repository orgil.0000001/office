using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Slide : Mb {
    Transform dogTf;
    [HideInInspector]
    public bool isDog = false;
    float t = 0, t2 = 0;
    private void Start() {
        dogTf = go.Child(0);
    }
    private void Update() {
        if (IsPlaying && isDog && !Player.I.isMvSlide) {
            t += Dt;
            if (t > Ls.I.dogCrtTm) {
                t = 0;
                CrtDog();
            }
            t2 += Dt;
            if (t2 > Ls.I.dogBarkTm) {
                t2 = 0;
                Ac.I.Play("Bark" + Rnd.RngIn(1, 5));
            }
        }
    }
    void CrtDog() {
        Dog dog = Ins(Ls.I.dogPf, dogTf.TfPnt(V3.X(Rnd.F1 * 0.2f)), dogTf.rotation, Ls.I.dogsTf);
        dog.Init();
    }
    public void Init() {
        if (!isDog) {
            isDog = true;
        }
    }
}
