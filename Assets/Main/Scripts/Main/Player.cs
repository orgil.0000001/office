﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;
using System.IO;
using System.Linq;

public class Player : Character {
    public static Player I {
        get {
            if (_.Null())
                _ = GameObject.FindObjectOfType<Player>();
            return _;
        }
    }
    static Player _;
    [HideInInspector] public int dogCnt = 0;
    RaycastHit hit;
    [HideInInspector] public bool isMvSlide = false;
    private void Awake() {
        _ = this;
    }
    void Start() {
    }
    void Update() {
        if (IsPlaying) {
            if (IsMbD)
                SlideStart();
            if (!isMvSlide && IsMbU)
                SlideStop();
        }
    }
    public void SlideStart() {
        Ls.I.slide.isDog = true;
    }
    public void SlideStop() {
        if (Ls.I.slideIdx < Ls.I.l.tfs.Count - 1) {
            Ls.I.slide.isDog = false;
            CrtTxt(!Ls.I.room || Ls.I.room.dogCnt < Ls.I.dogGood * 0.7f ? 0 : Ls.I.room.dogCnt < Ls.I.dogGood ? 1 : Ls.I.room.dogCnt < Ls.I.dogMax ? 2 : 3);
            StaCor(SlideCor(0.3f, 0.7f));
        }
    }
    public void Clk() {
        //Ray ray = Camera.main.ScreenPointToRay(Mp);
        //if (Physics.SphereCast(ray, 0.5f, out hit, 100, Lm.Door)) {
        //    Door door = hit.transform.Par<Door>();
        //    if (door) {
        //        GameObject txtGo = Ins(Ls.I.txtPf, Cc.I.hud.transform);
        //        txtGo.ChildShow(door.room.t < 0.6f ? 0 : door.room.t < 0.8f ? 1 : door.room.t < 1 ? 2 : 3);
        //        Dst(txtGo, 1);
        //        door.Open();
        //    }
        //}
        if (Ls.I.slide.isDog)
            if (Ls.I.room && Ls.I.room.dogCnt >= Ls.I.dogMin) {
                CrtTxt(Ls.I.room.dogCnt < Ls.I.dogGood ? 1 : Ls.I.room.dogCnt < Ls.I.dogMax ? 2 : 3);
                StaCor(SlideCor(0.3f, 0.7f));
            } else CrtTxt(0);
        Ls.I.slide.isDog = !Ls.I.slide.isDog;
        //if (!Ls.I.slide.isDog)
        //    Ls.I.slide.Init();
        //if (Physics.SphereCast(ray, 0.5f, out hit, 100, Lm.Silde)) {
        //    Slide slide = hit.transform.Par<Slide>();
        //    if (slide) {
        //        slide.Init();
        //    }
        //}
    }
    void CrtTxt(int i) {
        GameObject txtGo = Ins(Ls.I.txtPf, Cc.I.hud.transform);
        txtGo.ChildShow(i);
        Dst(txtGo, 1);
    }
    IEnumerator SlideCor(float staTm, float tm) {
        isMvSlide = true;
        yield return Wf.S(staTm);
        //if (Ls.I.slideIdx + 1 == Ls.I.l.tfs.Count) {
        //    //Ls.I.bosses.ForEach(x => x.Happy());
        //    //Gc.I.LevelCompleted();
        //} else {
        for (float t = 0; t < tm; t += Dt) {
            Ls.I.SlidePos(V3.Lerp(Ls.I.l.tfs[Ls.I.slideIdx].Item1, Ls.I.l.tfs[Ls.I.slideIdx + 1].Item1, t / tm),
                Q.Lerp(Ls.I.l.tfs[Ls.I.slideIdx].Item2, Ls.I.l.tfs[Ls.I.slideIdx + 1].Item2, t / tm));
            yield return null;
        }
        Ls.I.SlidePos(Ls.I.l.tfs[Ls.I.slideIdx + 1].Item1, Ls.I.l.tfs[Ls.I.slideIdx + 1].Item2);
        Ls.I.slideIdx++;
        //}
        isMvSlide = false;
    }
    public void Reset() { }
}