﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum TutorialType { Line, Infinity, TapToPlay, InputName }

public class Cc : Singleton<Cc> { // Canvas Controller
    [Header("Panels")]
    public GameObject menu, hud, pause;
    public GameOver gameOverComplete, gameOverUnCompleted;
    public NewBest newBest;
    public LevelCompleted levelCompleted;
    [Header("Hud items")]
    public HudTime time;
    public FormatTxt kills, score, best, diamond, coin, life;
    public LevelBar levelBar;
    public LeaderBoard leaderBoard;
    public Text dogTxt;
    GameState tmpGs;
    InputField playerNameInp;
    float t = 0;
    private void Start() {
        Show(menu);
        Hide(hud);
        Hide(gameOverComplete);
        Hide(gameOverUnCompleted);
        Hide(newBest);
        Hide(levelCompleted);
        Hide(pause);
        ReadPlayerName();
    }
    private void Update() {
        if (IsPlaying) {
            if (leaderBoard)
                LeaderBoard(LeaderBoardData.GetDatas());
            if (time) {
                t += Dt;
                if (Gc.GameTime - t > 0)
                    Time(Gc.GameTime - t);
                else {
                    Time(0);
                    Gc.I.GameOver();
                }
            }
        }
    }
    void ReadPlayerName() {
        GameObject playerNameGo = GameObject.Find("PlayerName");
        if (playerNameGo) {
            playerNameInp = playerNameGo.Inp();
            if (playerNameInp)
                playerNameInp.text = Gc.PlayerName = Data.PlayerName.S();
        }
    }
    public void Score() { FtData(score, Gc.Score); }
    public void Score(float score) { FtData(this.score, score); }
    public void Best() { FtData(best, Gc.Best); }
    public void Best(float best) { FtData(this.best, best); }
    public void Coin() { FtData(coin, Gc.Coin); }
    public void Coin(int coin) { FtData(this.coin, coin); }
    public void Diamond() { FtData(diamond, Gc.Diamond); }
    public void Diamond(int diamond) { FtData(this.diamond, diamond); }
    public void Kills() { FtData(kills, Gc.Kills); }
    public void Kills(int kills) { FtData(this.kills, kills); }
    public void Life() { FtData(life, Gc.Life); }
    public void Life(int life) { FtData(this.life, life); }
    public void Time() { if (time) time.Data(t); }
    public void Time(float time) { if (this.time) this.time.Data(time); }
    public void LevelBar(int level = 1, float scoreT = 0, float bestT = 0, string txt = "", int stage = 1, bool isNxtLvlCol = true) { if (levelBar) levelBar.Data(level, scoreT, bestT, txt, stage, isNxtLvlCol); }
    public void LeaderBoard(List<LeaderBoardData> datas = null) { if (leaderBoard) leaderBoard.Data(datas); }
    public void ShowHud() {
        Hide(menu);
        Show(hud);
    }
    public void GameOver(float time = 0, float score = 0, float best = 0, int level = 1, List<LeaderBoardData> datas = null, bool isComplete = true) { StaCor(GameOverCor(time, score, best, level, datas, isComplete)); }
    IEnumerator GameOverCor(float time, float score, float best, int level, List<LeaderBoardData> datas, bool isComplete) {
        yield return Wf.S(time);
        if (isComplete) {
            if (gameOverComplete) {
                Hide(hud);
                Show(gameOverComplete);
                gameOverComplete.Data(score, best, level, datas);
            }
        } else {
            if (gameOverUnCompleted) {
                Hide(hud);
                Show(gameOverUnCompleted);
                gameOverUnCompleted.Data(score, best, level, datas);
            }
        }
    }
    public void NewBest(float time = 0, float score = 0, int level = 1, List<LeaderBoardData> datas = null) { StaCor(NewBestCor(time, score, level, datas)); }
    public IEnumerator NewBestCor(float time, float score, int level, List<LeaderBoardData> datas) {
        yield return Wf.S(time);
        if (newBest) {
            Hide(hud);
            Show(newBest);
            newBest.Data(score, level, datas);
        }
    }
    public void LevelCompleted(float time = 0, int level = 1, List<LeaderBoardData> datas = null) { StaCor(LevelCompletedCor(time, level, datas)); }
    IEnumerator LevelCompletedCor(float time, int level, List<LeaderBoardData> datas) {
        yield return Wf.S(time);
        if (levelCompleted) {
            Hide(hud);
            Show(levelCompleted);
            levelCompleted.Data(level, datas);
        }
    }
    public void Pause(float time = 0) { StaCor(PauseCor(time)); }
    IEnumerator PauseCor(float time) {
        yield return Wf.S(time);
        if (pause) {
            Show(pause);
            UnityEngine.Time.timeScale = 0;
            tmpGs = Gc.State;
            Gc.State = GameState.Pause;
        }
    }
    public void Resume(float time = 0) { StaCor(ResumeCor(time)); }
    IEnumerator ResumeCor(float time) {
        yield return Wf.S(time);
        if (pause) {
            Hide(pause);
            UnityEngine.Time.timeScale = 1;
            Gc.State = tmpGs;
        }
    }
    public void Replay(float time = 0) { StaCor(ReplayCor(time)); }
    IEnumerator ReplayCor(float time) {
        yield return Wf.S(time);
        UnityEngine.Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
    public void Play() {
        if (playerNameInp)
            Gc.PlayerName = playerNameInp.text;
        Data.PlayerName.Set(Gc.PlayerName);
        LeaderBoardData.SetPlayerData(Gc.PlayerName);
        Gc.I.Play();
    }
    void FtData(FormatTxt txt, float data) { if (txt) txt.Data(data); }
    void Hide(GameObject a) { if (a) a.Hide(); }
    void Show(GameObject a) { if (a) a.Show(); }
    void Hide(Component a) { if (a) a.Hide(); }
    void Show(Component a) { if (a) a.Show(); }
}