﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using Orgil;

[System.Serializable]
public class Sound {
    public string name;
    public AudioClip clip;
    [Range(0f, 1f)]
    public float volume = 1;
    [Range(-3f, 3f)]
    public float pitch = 1;
    public bool isLoop;
    [HideInInspector]
    public AudioSource source;
}

public class Ac : Singleton<Ac> { // Audio Controller
    public List<Sound> sounds;
    private void Awake() {
        foreach (Sound s in sounds) {
            s.source = go.Ac<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.isLoop;
        }
    }
    public void Play(string name) {
        Sound s = sounds.Find(x => x.name == name);
        if (s.NotNull())
            s.source.Play();
    }
}
