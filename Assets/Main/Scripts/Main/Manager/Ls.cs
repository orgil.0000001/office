﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

[System.Serializable]
public class Env {
    public Color wall, door, doorOpen, floor, slide, slideBody;
}

public class Level {
    public List<string> data = new List<string>();
    public List<(Vector3, Quaternion)> tfs = new List<(Vector3, Quaternion)>();
    public Level(List<string> l, params (float, float, float)[] a) {
        data = l;
        for (int i = 0; i < a.Length; i++)
            tfs.Add((V3.Xz(a[i].Item1, a[i].Item2), Q.Y(a[i].Item3)));
    }
}

public class Ls : Singleton<Ls> { // Level Spawner
    public float exp = 1000;
    public GameObject txtPf;
    [Header("Bot")]
    public float emojiMinTm = 3;
    public float emojiMaxTm = 5, botHappyPct = 0.3f, botHappy2Pct = 0.7f;
    public Color botDefC, botHappyC, botHappy2C;
    [Header("Slide")]
    public Slide slidePf;
    [Header("Room")]
    public Room roomPf;
    [Header("Wall")]
    public Wall wallPf;
    public AnimationCurve wallHorAc, wallVerAc;
    public float wallPwr = 1, wallH = 4, wallW = 0.5f;
    public Vector3Int wallN = V3I.V(20, 20, 1);
    public Color wallMaxC;
    [Header("Floor")]
    public float floorH = 0.2f;
    [Header("Door")]
    public Door doorPf;
    public float doorW = 1, doorOpenTm = 1, doorOpenPct = 0.5f, doorColTm = 0.5f;
    [Header("Dog")]
    public Dog dogPf;
    public int dogMax, dogMin, dogGood;
    [HideInInspector] public int dogCnt;
    public float dogCrtTm = 0.2f, dogBarkTm = 0.5f;
    public List<Env> envs = new List<Env>();
    List<Level> levels = List(
        //new Level("b0 wowo4 swwd3"), // -
        //new Level("wsdw1", "odww3 wwoo4p", "= b0"), // s
        //new Level("= wwds4", "woww1p oowd2 b0"), // ┴
        //new Level("woww3p wood4p b0", "= dwow2", "= dwws4"), // T
        //new Level("b0p = wwow4p", "owow3 = dwow4", "doww1 sdwd2 dwwo1"), // U

        //new Level("b0 wwoo1p", "= dsdw3p", "= odww1 wwwo2"), // z
        //new Level("swdw4 = wwow4", "odww1p wdoo2p dwwo1p", "= b0"), // Y
        //new Level("wwow3p = wwow4p", "doow2 sdwd1 dwdo4", "b0p = owww2p"), // H
        //new Level("sddw2 wdwo1 wwdo4p", "owdw4 = owow0p", "odww1p wowo2p b0p"), // O
        //new Level("= b0p", "woww3p odod3 wwwo4p", "= dsww1p"), // +

        //new Level("wsdw1p", "oddw3p wdwo4 wwoo4", "owww1p = b0p"), // h
        //new Level("woow3 b0p", "dwow1", "doww3p wowd1p swwd2p"), // c
        //new Level("woww2 wdod1p wwwo4", "= dsdw3", "= owdw1", "b0 odwo3p wwwo2"), // I
        //new Level("wwow3p = wwow4p", "dwow3 = dwow4", "doww1p sdwd2 dwdo2", "= = owdw4", "b0p wowo3p owwd2p"), // y
        //new Level("= = wsdw3p", "= = owdw0", "b0p wowo1p oddd0 wdwo0 wwwo4p", "= = owdw0", "= = owww2p") // ┼
        new Level(List("b0 wowo4 wwwd3"), (8, 2, 0), (4, 2, 0)), // -
        new Level(List("wwdw1", "odww3 wwoo4p", "= b0"), (2, 8, 90), (2, 6, 45), (4, 6, 0)), // s
        new Level(List("= wwdw4", "woww1p oowd2 b0"), (2, 4, -90), (2, 2, -45), (0, 2, 0)), // ┴
        new Level(List("woww3p wood4p b0", "= dwow2", "= dwww4"), (2, 0, -90), (2, 4, -90), (2, 6, -135), (0, 6, -180)), // T
        new Level(List("b0p = wwow4p", "owow3 = dwow4", "doww1 wdwd2 dwwo1"), (7.5f, 8, -90), (7.5f, 4, -90), (6.5f, 1.5f, -45), (4, 1.5f, 0), (1.5f, 1.5f, 45), (0.5f, 4, 90)), // U

        new Level(List("b0 wwoo1p", "= dwdw3p", "= odww1 wwwo2"), (6, 8, 90), (6, 4, 90), (6, 2, 45), (8, 2, 0)), // z
        new Level(List("wwdw4 = wwow4", "odww1p wdoo2p dwwo1p", "= b0"), (0.5f, 8, 90), (1.5f, 5.5f, 45), (4, 5.5f, 0), (6.5f, 5.5f, -45), (7.5f, 8, -90)), // Y
        new Level(List("wwow3p = wwow4p", "doow2 wdwd1 dwdo4", "b0p = b2p"), (0.5f, 8, 90), (1.5f, 5.5f, 45), (4, 5.5f, 0), (6.5f, 5.5f, -45), (7.5f, 8, -90)), // H
        new Level(List("wddw2 wdwo1 wwdo4p", "owdw4 = owow0p", "odww1p wowo2p b0p"), (4, 0.5f, 0), (1.5f, 1.5f, 45), (0.5f, 4, 90), (1.5f, 6.5f, 135), (4, 7.5f, 180), (6.5f, 6.5f, 225), (7.5f, 4, 270)), // O
        new Level(List("= wwow0", "woww3p b0p wwwo4p", "= owww1p"), (4, 10, 0), (10, 4, 90), (4, -2, 180), (-2, 4, 270)), // +

        new Level(List("wwdw1p", "odow3p wdwo4 wwoo4", "b0p = dwww1p"), (2, 8, 90), (2, 6, 45), (4, 6, 0), (8, 6, 0), (10, 0, 90)), // h
        new Level(List("woow3 b0p", "dwow1", "doww3p wowd1p wwwd2p"), (1.5f, 6.5f, 135), (1.5f, 4, 90), (1.5f, 1.5f, 45), (4, 1.5f, 0), (8, 1.5f, 0)), // c
        new Level(List("woww2 wdod1p wwwo4", "= dwdw3", "= owdw1", "b0 odwo3p wwwo2"), (0, 14, 0), (4, 14, 0), (10, 12, 90), (6, 8, 90), (6, 4, 90), (6, 2, 45), (8, 2, 0)), // I
        new Level(List("wwow3p = wwow4p", "dwow3 = dwow4", "doww1p wdwd2 dwdo2", "= = owdw4", "b0p wowo3p owwd2p"), (4, 0.5f, 0), (6.5f, 1.5f, -45), (6.5f, 4, -90), (6.5f, 6.5f, -135), (4, 6.5f, -180), (0, 6.5f, -180), (-2, 12, -90), (0, 18, 0), (8, 18, 0), (10, 12, 90)), // y
        new Level(List("= = wwdw3p", "= = owdw0", "b0p wowo1p oddd0 wdwo0 wwwo4p", "= = owdw0", "= = owww2p"), (4, 10, 0), (6, 10, -45), (6, 12, -90), (8, 18, 0), (12, 10, 0), (18, 8, 90), (10, 4, 90), (10, 0, 90)) // ┼
    );
    public bool useLvl = false;
    public int lvl = 1;
    int Lvl => GetLvl(Gc.Level, levels.Count, 1, levels.Count);
    int LvlIdx => Lvl - 1;
    [HideInInspector] public Transform dogsTf;
    [HideInInspector] public int happCnt = 0;
    [HideInInspector] public List<Bot> bots = new List<Bot>();
    [HideInInspector] public List<Bot> bosses = new List<Bot>();
    [HideInInspector] public Slide slide;
    [HideInInspector] public Level l;
    [HideInInspector] public int slideIdx = 0;
    [HideInInspector] public Room room;
    Env e;
    Vector2 sz = V2.V(4, 4);
    Vector3 min, max, phoneDp = V3.Xz(0.7f), pcDp = V3.Xz(0.9f, 0.7f);
    CapsuleCollider dogCc;
    public void Init() {
        if (useLvl)
            Gc.Level = lvl;
        LoadLevel();
        LeaderBoardData.SetDatas();
    }
    public void LoadLevel() {
        dogsTf = go.Child(1);
        dogCc = dogPf.Cc();
        l = levels[LvlIdx];
        e = envs[LvlIdx % envs.Count];
        Vector2Int n = V2I.Y(l.data.Count);
        for (int i = 0; i < l.data.Count; i++) {
            string[] arr = l.data[i].RgxSplitSpc();
            n.x = M.Max(n.x, arr.Length);
            for (int j = 0; j < arr.Length; j++)
                if (arr[j] != "=")
                    CrtRoom(V3.Xz(j * sz.x, (l.data.Count - i - 1) * sz.y), arr[j]);
        }
        CrtSlide(l.tfs[0].Item1, l.tfs[0].Item2);
        dogCnt = l.tfs.Count * dogGood;
        Cc.I.dogTxt.text = "" + dogCnt;
        Vector3 cen = V3.Xz((n.x - 1) / 2f * sz.x, (n.y - 1) / 2f * sz.y);
        min = cen - V3.Xz(sz.x * (n.x / 2f + 1), sz.y * n.x * 1.2f);
        max = cen + V3.Xz(sz.x * (n.x / 2f + 1), sz.y * n.x * 2.4f);
        Cm.I.Tp = cen + Cm.I.B * (n.x + 1) * sz.x * 1.7f;
        CrtEnv();
    }
    static List<string> EnvPaths = List("Bush/Bush_temp_climate_", "Desert plant/Desert_plant_", "Bush winter/Bush_winter_", "Stone/Middle/Stone_mid_", "", "Tree/Tree_temp_climate_", "Desert plant/Desert_plant_", "Tree winter/Tree_Winter_", "Stone/Big/Stone_big_", "");
    static List<int> EnvNums = List(13, 20, 20, 10, 0, 21, 20, 20, 10, 0);
    void CrtEnv() {
        int env = LvlIdx % 5, env2 = LvlIdx % 10, n = env2 == 5 || env2 == 7 || env2 == 8 ? 25 : 50;
        go.ChildShow(0, 0, env);
        if (env < 4) {
            Transform plantsTf = go.Child(0, 1);
            for (int i = 0; i < n; i++) {
                GameObject pf = PlantRndPf(env2);
                Ins(pf, PlantRndPos(), pf.Tr(), plantsTf);
            }
        }
    }
    Vector3 PlantRndPos() {
        Vector3 pos = default;
        for (int i = 0; i < 1000; i++) {
            pos = Rnd.Pos(min, max);
            if (!Physics.CheckSphere(pos, 3, Lm.Floor))
                return pos;
        }
        return pos;
    }
    GameObject PlantRndPf(int env2) {
        int num = Rnd.RngIn(1, EnvNums[env2]);
        GameObject pf = A.LoadGo(EnvPaths[env2] + (num < 10 ? "00" : "0") + num);
        return pf;
    }
    void CrtRoom(Vector3 pos, string s) {
        Room room = Ins(roomPf, pos, Q.O, tf);
        Tf pTf = room.tf.Tf();
        room.Bc(V3.V(sz.x, 0.02f, sz.y), V3.Y(0.01f), true, null, GcTp.Add);
        room.Child(0).Tls(sz.x, floorH, sz.y);
        bool isBoss = s[0] == 'b', isPc = s[s.Length - 1] == 'p';
        int p = s[s.Length - (isPc ? 2 : 1)].S().I();
        Vector3 bp = sz.Xz() / 2f - (isPc ? pcDp : phoneDp);
        Bot bot = Bs.I.Create(pTf.Pnt(p == 0 ? V3.O : p == 1 ? bp.Nzn() : p == 2 ? bp.Pzn() : p == 3 ? bp.Nzp() : bp), V3.Y(p == 1 || p == 2 ? 0 : 180));
        bot.Upd(isBoss, isPc, room);
        (isBoss ? bosses : bots).Add(bot);
        s = isBoss ? "wwww" : s;
        CrtWall(pTf * Tf.PRS(V3.Z(sz.y / 2), 0, V3.V(sz.x, wallH, wallW)), room.tf, s[0]);
        CrtWall(pTf * Tf.PRS(V3.X(sz.x / 2), 90, V3.V(sz.y, wallH, wallW)), room.tf, s[1]);
        CrtWall(pTf * Tf.PRS(V3.Z(-sz.y / 2), 180, V3.V(sz.x, wallH, wallW)), room.tf, s[2]);
        CrtWall(pTf * Tf.PRS(V3.X(-sz.x / 2), 270, V3.V(sz.y, wallH, wallW)), room.tf, s[3]);
        //int dogMax = M.RoundI((sz.x * sz.y - (isPc ? 3 : 1)) / (dogCc.height * dogCc.radius * 2));
        room.Init(e.wall, e.door, e.doorOpen, e.floor, dogMax, bot);
    }
    void CrtWall(Tf pTf, Transform parTf, char c) {
        if (c == 'w' || c == 's') {
            Wall wall = Ins(wallPf, pTf.p, pTf.q, parTf);
            wall.Tls = pTf.s + V3.X(wallW);
            //if (c == 's') CrtSlide(pTf.p, pTf.q);
        } else if (c == 'd') {
            Door door = Ins(doorPf, pTf.p, pTf.q, parTf);
            door.Tls = pTf.s.Z(doorW) - V3.X(wallW);
        }
    }
    void CrtSlide(Vector3 p, Quaternion r) {
        slide = Ins(slidePf, p + V3.Y(wallH), r * Q.Y(180), tf);
        slide.Child(1).RenMatCol(e.slide);
        slide.Child(2).RenMatCol(e.slideBody);
        slide.Child(3).RenMatCol(e.slideBody);
    }
    public void SlidePos(Vector3 p, Quaternion r) {
        slide.Tp = p + V3.Y(wallH);
        slide.Tr = r * Q.Y(180);
    }
    int GetLvl(int lvl, int lvlCnt, int rnd1, int rnd2) {
        if (Data.LevelData.S().IsNe())
            Data.LevelData.SetList(A.ListAp(1, 1, lvlCnt));
        List<int> lis = Data.LevelData.ListI();
        if (lvl <= lis.Count) {
            return lis[lvl - 1];
        } else {
            int prvLvl = lis.Last();
            for (int i = lis.Count + 1; i <= lvl; i++) {
                prvLvl = prvLvl == rnd1 ? Rnd.RngIn(rnd1 + 1, rnd2) : prvLvl == rnd2 ? Rnd.RngIn(rnd1, rnd2 - 1) : Rnd.P((prvLvl - rnd1).F() / (rnd2 - rnd1)) ? Rnd.RngIn(rnd1, prvLvl - 1) : Rnd.RngIn(prvLvl + 1, rnd2);
                lis.Add(prvLvl);
            }
            Data.LevelData.SetList(lis);
            return prvLvl;
        }
    }
}