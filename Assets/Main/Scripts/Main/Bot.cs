﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public enum EmojiTp { Happy1, Happy2, Smile, Love, Hearts, Stars, Sad, Hurt, Angry }

public class Bot : Character {
    Room room;
    bool isBoss, isPc, isHappy = false, isHappy2 = false;
    float t = 0, tm;
    List<Material> mats;
    Color c;
    private void Start() {
        Init();
        tm = Rnd.Rng(Ls.I.emojiMinTm * 0.3f, Ls.I.emojiMinTm);
    }
    private void Update() {
        if (!isBoss) {
            UpdCol(C.Lerp(c, isHappy2 ? Ls.I.botHappy2C : isHappy ? Ls.I.botHappyC : Ls.I.botDefC, Dt * 10));
            if (IsPlaying) {
                if (!isHappy && room.t > Ls.I.botHappyPct) {
                    isHappy = true;
                    Happy();
                }
                if (!isHappy2 && room.t > Ls.I.botHappy2Pct) {
                    isHappy2 = true;
                    Ls.I.happCnt++;
                    Cc.I.LevelBar(Gc.Level, Ls.I.happCnt.F() / Ls.I.bots.Count);
                    //if (Ls.I.happCnt == Ls.I.bots.Count) {
                    //    Ls.I.bosses.ForEach(x => x.Happy());
                    //    Gc.I.LevelCompleted();
                    //}
                }
                t += Dt;
                if (t > tm) {
                    t = 0;
                    tm = Rnd.Rng(Ls.I.emojiMinTm, Ls.I.emojiMaxTm);
                    Emoji(isHappy2 ? Rnd.Arr(EmojiTp.Hearts, EmojiTp.Stars) : isHappy ? Rnd.Arr(EmojiTp.Happy1, EmojiTp.Happy2, EmojiTp.Smile, EmojiTp.Love) : Rnd.Arr(EmojiTp.Sad, EmojiTp.Hurt));
                }
            }
        }
    }
    public void Happy() { an.Play("Dance"); }
    public void Death() { an.Play("Death"); }
    public void Angry() { an.Play("Angry"); Emoji(EmojiTp.Angry); }
    public void Upd(bool isBoss, bool isPc, Room room) {
        this.room = room;
        this.isBoss = isBoss;
        this.isPc = isPc;
        mats = go.Child(0, 0).RenMats();
        an = go.ChildGo(0).An();
        if (isBoss) {
            Tls *= 1.3f;
            mats[0].color = C.black;
            mats[1].color = C.white;
            go.ChildShow(0, 1, 2, 0, 0, 1, 0, 0);
        } else {
            UpdCol(Ls.I.botDefC);
        }
        if (isPc) {
            an.Play("Type");
            go.ChildShow(1);
        } else {
            an.Play("Talk");
            go.ChildShow(0, 1, 2, 0, 0, 0, 0, 0, 0, 0);
        }
    }
    void UpdCol(Color c) { this.c = c; mats.ForEach(x => x.color = c); }
    public void Emoji(EmojiTp tp) {
        GameObject eGo = Ins(A.LoadGo("Emoji"), Tp, Q.O, tf), cGo = eGo.ChildGo(0);
        cGo.Tr(Cm.I.Tr);
        cGo.RenMat().SetTexture("_MainTex", A.Load<Texture>("Emoji/" + tp));
        Dst(eGo, 2f);
    }
}