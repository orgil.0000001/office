using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Dog : Mb {
    Animator an;
    bool isChg = false;
    public void Init() {
        GameObject mGo = go.ChildGo(0, Rnd.Idx(go.Child(0).Tcc()));
        mGo.Show();
        an = mGo.An();
        an.Play("Sit");
        rb.mass = 1000;
    }
    private void Update() {
        if (!isChg && Tp.y < Ls.I.wallH) {
            go.layer = Lyr.Default;
            isChg = true;
        }
    }
    private void OnTriggerEnter(Collider other) {
        if (other.Tag(Tag.Room)) {
            rb.mass = 1;
            rb.Con(false, true, false, true, false, true);
            Tp = Tp.Y(0);
        }
    }
}
