using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Room : Mb {
    [HideInInspector]
    public Bot bot;
    Color wallC;
    List<Wall> walls = new List<Wall>();
    List<Door> doors = new List<Door>();
    public List<Dog> dogs = new List<Dog>();
    int dogMax;
    public float t => dogCnt.F() / dogMax;
    //public bool isDone => t > 0.8f;
    [HideInInspector] public int dogCnt = 0;
    private void Start() {
    }
    public void Init(Color wallC, Color doorC, Color doorOpenC, Color floorC, int dogMax, Bot bot) {
        this.wallC = wallC;
        this.dogMax = dogMax;
        this.bot = bot;
        List<GameObject> gos = go.ChildGos();
        gos[0].Child(0).RenMatCol(floorC);
        for (int i = 1; i < gos.Count; i++) {
            Wall wall = gos[i].Gc<Wall>();
            if (wall) {
                wall.Upd(0, wallC);
                walls.Add(wall);
            } else {
                Door door = gos[i].Gc<Door>();
                if (door) {
                    door.Init(doorC, doorOpenC, this);
                    doors.Add(door);
                }
            }
        }
    }
    void UpdWall() {
        if (t > Ls.I.botHappyPct)
            walls.ForEach(x => x.Upd(t, C.Lerp(wallC, Ls.I.wallMaxC, (t - Ls.I.botHappyPct) / (1 - Ls.I.botHappyPct))));
    }
    private void OnTriggerEnter(Collider other) {
        if (IsPlaying)
            if (other.Tag(Tag.Dog)) {
                Ls.I.room = this;
                dogCnt++;
                Player.I.dogCnt++;
                Cc.I.dogTxt.text = "" + M.C(Ls.I.dogCnt - Player.I.dogCnt, 0, 10000);
                dogs.Add(other.Gc<Dog>());
                UpdWall();
                if (t > 1.2f && walls.IsCount()) {
                    Gc.I.GameOver();
                    DstRoom();
                } else if (Player.I.dogCnt == Ls.I.dogCnt) {
                    Ls.I.bosses.ForEach(x => x.Happy());
                    Gc.I.LevelCompleted();
                }
            }
    }
    private void OnTriggerExit(Collider other) {
        if (IsPlaying)
            if (other.Tag(Tag.Dog)) {
                dogCnt--;
                dogs.Rmv(other.Gc<Dog>());
                UpdWall();
            }
    }
    public void DstRoom() {
        Vector3 pos = Tp + V3.Y(-10);
        foreach (Wall wall in walls) {
            wall.Rb(GcTp.Add);
            Dst(wall.cGo.Bc());
            MeshCollider mc = wall.cGo.Mc(GcTp.Add);
            mc.sharedMesh = wall.mesh;
            mc.convex = true;
            wall.rb.G();
            wall.rb.AddExplosionForce(Ls.I.exp, pos, 20);
        }
        foreach (Dog dog in dogs) {
            dog.ColMat(null);
            dog.rb.Con();
            dog.rb.V0();
            dog.rb.AddExplosionForce(Ls.I.exp, pos, 20);
        }
        Ac.I.Play("Exp");
        bot.Death();
        Ls.I.bosses.ForEach(x => x.Angry());
    }
}
