using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Wall : Mb {
    [Range(0, 1)]
    public float t;
    public bool isUpd = false;
    [HideInInspector]
    public Mesh mesh = null;
    [HideInInspector]
    public GameObject cGo;
    List<Vector3> vs;
    Material mat;
    void Update() {
        if (isUpd)
            Upd(t);
    }
    public void Upd(float t, Color c = default) {
        if (!mesh) {
            cGo = go.ChildGo(0);
            mat = cGo.RenMat();
            Msh.Init(ref mesh, cGo);
            Cube(ref mesh, Ls.I.wallN);
            vs = mesh.vertices.List();
        }
        List<Vector3> vs2 = vs.Copy();
        for (int i = 0; i < vs.Count; i++)
            vs2[i] += V3.Z(Ls.I.wallHorAc.Evaluate(vs[i].x + 0.5f) * Ls.I.wallVerAc.Evaluate(vs[i].y + 0.5f) * t * Ls.I.wallPwr / Tls.z);
        mesh.vertices = vs2.Arr();
        mesh.RecalculateNormals();
        if (!c.IsDef())
            mat.color = c;
    }
    void Cube(ref Mesh mesh, Vector3Int n) {
        Vector3Int m = n + V3I.I;
        Msh msh = new Msh(true);
        for (int v = 0; v < 3; v++) {
            System.Func<int, int, int, int> f = new System.Func<int, int, int, int>[] {
                (i, j, k) => i * m.y * m.x + j * m.x + k,
                (i, j, k) => i * 2 * m.x + j * m.x + k + 2 * m.y * m.x,
                (i, j, c) => i * m.y * 2 + j * 2 + c + 2 * m.y * m.x + m.z * 2 * m.x
            }[v];
            System.Action<int, int, int> f2 = new System.Action<int, int, int>[] {
                (i, j, k) => msh.ts.Fc(i == 0, f(i, j, k), f(i, j + 1, k), f(i, j + 1, k + 1), f(i, j, k + 1)),
                (i, j, k) => msh.ts.Fc(j == 1, f(i, j, k), f(i + 1, j, k), f(i + 1, j, k + 1), f(i, j, k + 1)),
                (i, j, k) => msh.ts.Fc(k == 1, f(i, j, k), f(i, j + 1, k), f(i + 1, j + 1, k), f(i + 1, j, k))
            }[v];
            Vector3Int vi = Arr(V3I.V(n.x, n.y, 1), V3I.V(n.x, 1, n.z), V3I.V(1, n.y, n.z))[v],
                ti = Arr(V3I.V(n.x, n.y, 2), V3I.V(n.x, 2, n.z), V3I.V(2, n.y, n.z))[v];
            for (int i = 0; i <= vi.z; i++)
                for (int j = 0; j <= vi.y; j++)
                    for (int k = 0; k <= vi.x; k++)
                        msh.vs.Add(V3.V(k.F() / vi.x - 0.5f, j.F() / vi.y - 0.5f, i.F() / vi.z - 0.5f));
            for (int i = 0; i < ti.z; i++)
                for (int j = 0; j < ti.y; j++)
                    for (int k = 0; k < ti.x; k++)
                        f2(i, j, k);
        }
        msh.Upd(ref mesh);
    }
}
