using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Door : Mb {
    [HideInInspector]
    public Room room;
    bool isOpen = false, isCol = false;
    Color c, openC;
    float t = 0;
    Material mat;
    private void Update() {
        //if (IsPlaying && room.t > Ls.I.doorOpenPct && !isCol) {
        //    t += Dt;
        //    mat.color = C.Lerp(c, openC, t / Ls.I.doorColTm);
        //    if (t > Ls.I.doorColTm)
        //        isCol = true;
        //}
    }
    public void Init(Color c, Color openC, Room room) {
        this.c = c;
        this.openC = openC;
        this.room = room;
        mat = go.Child(0).RenMat();
        mat.color = c;
    }
    public void Open() {
        if (!isOpen && isCol)
            StaCor(OpenCor());
    }
    IEnumerator OpenCor() {
        isOpen = true;
        Tp = Tp.Y(-0.01f);
        for (float t = 0; t < Ls.I.doorOpenTm; t += Dt) {
            Tls = Tls.Y(M.Lerp(Ls.I.wallH, 0, t / Ls.I.doorOpenTm));
            yield return null;
        }
        Tls = Tls.Y(0.001f);
    }
}
